require 'container'

class Component
  # All implementors must define these methods
  def self.package_name
    raise "This component's package name has not been configured."
  end

  def self.configure(&block)
    block.call unless block.nil?
  end

  def self.start(&block)
    block.call unless block.nil?
  end

  def self.stop(&block)
    block.call unless block.nil?
  end

  def self.uninstall_dependencies(&block)
    block.call unless block.nil?
  end

  def self.install_dependencies(&block)
    unless Container.is_baked? package_name
      block.call unless block.nil?
      Container.bake_ami package_name
    end
  end

  def self.reinstall_dependencies(&block)
    block.call unless block.nil?
  end
end
