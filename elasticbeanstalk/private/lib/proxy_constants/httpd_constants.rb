class HttpdConstants
  PROXY_NAMESPACE = 'aws:elasticbeanstalk:environment:proxy'
  PROXY_SERVER = 'ProxyServer'
  PROXY_SERVER_APACHE = 'apache'
  PROXY_SERVER_APACHE_22 = 'apache/2.2'

  def self.proxy_type
    begin
      Container.config.optionsetting(PROXY_NAMESPACE, PROXY_SERVER).strip
    rescue ElasticBeanstalk::BeanstalkRuntimeError
      Container.container_config('proxy_server').strip
    end
  end

  def self.container_staging_directory
    Container.container_staging_directory
  end

  def self.proxy_staging_directory
    "#{container_staging_directory}/proxy"
  end

  def self.port_mappings_file
    "#{proxy_staging_directory}/mappings"
  end

  def self.httpd_staging_directory
    "#{container_staging_directory}/httpd"
  end
  def self.httpd_staging_conf_directory
    "#{httpd_staging_directory}/conf"
  end

  def self.httpd_staging_confd_directory
    "#{httpd_staging_directory}/conf.d"
  end

  def self.eb_httpd_staging_config
    "#{httpd_staging_directory}/conf/httpd.conf"
  end

  def self.httpd_eb_conf_staging_directory
    "#{httpd_staging_directory}/conf.d/elasticbeanstalk"
  end

  def self.eb_httpd_app_locations_file
    "#{httpd_eb_conf_staging_directory}/00_application.conf"
  end

  def self.eb_httpd_static_locations_file
    "#{httpd_eb_conf_staging_directory}/02_static.conf"
  end

  def self.eb_httpd_gzip_file
    "#{httpd_eb_conf_staging_directory}/01_gzip.conf"
  end

  def self.proxy_server_apache
    return PROXY_SERVER_APACHE
  end

  def self.proxy_server_apache_22
    return PROXY_SERVER_APACHE_22
  end
end