require 'elasticbeanstalk/manifest'
require 'elasticbeanstalk/exceptions'
require 'elasticbeanstalk/environment'

require 'fileutils'
require 'container'
require 'component'
require 'json'

class XRayComponent < Component
  PACKAGE_NAME = 'XRay'

  # Tracing daemon config
  EB_XRAY_DAEMON_CONF = '/opt/elasticbeanstalk/private/config/supervisord/xray.conf'
  # Location of the X-Ray daemon supervisord config
  XRAY_DAEMON_CONF = '/etc/supervisor/conf.d/xray.conf'
  # Location of the SDK environment details file
  XRAY_SDK_CONFIG_DIR = '/var/elasticbeanstalk/xray'
  XRAY_SDK_CONFIG = File.join(XRAY_SDK_CONFIG_DIR, 'environment.conf')
  XRAY_LOG_DIRECTORY = '/var/log/xray'
  
  def self.start
    super do
      begin
        if enabled?
          Container.initctl_start("xray")
            
          # Healthd support
          FileUtils.ln_s('/var/run/xray.pid', '/var/elasticbeanstalk/healthd/xray.pid')
        end
      rescue => exception
        # To eb-activity log
        puts(ElasticBeanstalk.format_exception(exception))
      end
    end
  end

  def self.stop
    super do
      begin
        Container.initctl_stop("xray")
        
        # Healthd support
        FileUtils.rm_f('/var/elasticbeanstalk/healthd/xray.pid')
      rescue => exception
        # To eb-activity log
        puts(ElasticBeanstalk.format_exception(exception))
      end
    end
  end

  def self.configure
    super do
      if enabled?
        if ! instance_profile_specified?
          return Container.emit_warning('An instance profile is required in order to integrate with the AWS X-Ray service.')
        end

        create_config
        configure_logging
      end
    end
  end

  def self.install_dependencies
    super do
      rpm_location = '/opt/elasticbeanstalk/private'
      rpm_name = 'aws-xray-daemon.rpm'
      Container.download_bundled_app(rpm_name, rpm_location)

      begin
        Container.yum_install_rpms(package_name, File.join(rpm_location, rpm_name))
      rescue => exception
        # To eb-activity log
        puts(ElasticBeanstalk.format_exception(exception))
        # To event stream
        Container.emit_warning('Unable to install the X-Ray daemon.')
      end
    end

    if ! File.exists? XRAY_LOG_DIRECTORY
      FileUtils.mkdir(XRAY_LOG_DIRECTORY)
      Container.run_command("/bin/chown -R xray:xray #{XRAY_LOG_DIRECTORY}")
    end
  end

  private

  def self.instance_profile_specified?
    ElasticBeanstalk::EnvironmentMetadata.new(logger: Logger.new(STDOUT)).instance_profile_specified?
  end

  def self.enabled?
    Container.container_config("xray_enabled") == "true"
  end

  def self.configure_logging
    Container.configure_publish_logs('awsxray', File.join(XRAY_LOG_DIRECTORY, 'xray.log'))
  end

  def self.create_config
    manifest = ElasticBeanstalk::Manifest.load_cache(logger: Logger.new(STDOUT))
    environment_name = Container.container_config("environment_name")

    xray_sdk_config = {
      "deployment_id" => manifest.deployment_id,
      "version_label" => manifest.version_label,
      "environment_name" => environment_name
    }

    FileUtils.mkdir_p(XRAY_SDK_CONFIG_DIR)
    IO.write(XRAY_SDK_CONFIG, JSON.dump(xray_sdk_config))
  end

  def self.package_name
    PACKAGE_NAME
  end
end
