require 'open3'
require 'net/http'
require 'open-uri'

class Container
  MANIFEST_PATH = '/etc/elasticbeanstalk/baking_manifest'
  FILE_MAX = '/proc/sys/fs/file-max'
  # Minimum number of max open files
  MIN_ULIMIT = 4096
  # Maximum number of max open files
  MAX_ULIMIT = 200000
  CONTAINER_CONFIGURATION = '/opt/elasticbeanstalk/deploy/configuration/containerconfiguration'
  BAKING_CONFIGURATION = '/tmp/tmp_config/baking_config.json'

  def self.user_add(user, shell='/sbin/nologin')
    run_command("if ! /usr/bin/id -u #{user}; then /usr/sbin/useradd --system --user-group #{user} --create-home -s #{shell}; fi")
  end

  def self.yum_force_install(*packages)
    # Create an SIM for testing "yum install-n"
    # https://issues.amazon.com/issues/BEANSTALK-15579
    run_command("/usr/bin/yum -y install #{packages.join(' ')}")
  end

  # This method enables the installation of rpm via install
  def self.yum_install_rpms(manifest, *rpms)
    with_validation manifest, rpms do
      run_command("/usr/bin/yum -y localinstall #{rpms.join(' ')}")
    end
  end

  # This method enables the installation of packages via yum.
  def self.yum_install_packages(manifest, *packages)
    with_validation manifest, packages do
      yum_force_install packages
    end
  end

  def self.reinstall_packages(*packages)
    validate_packages packages
    yum_force_install packages
  end

  # This method enables the installation of packages via yum groups
  def self.yum_group_install(manifest, *packages)
    with_validation manifest, packages do
      packages.each do |package|
        run_command("/usr/bin/yum -y groupinstall '#{package}'")
      end
    end
  end

  def self.yum_installed?(package)
    _, status = run_command("yum list installed #{package}", false, false)
    return status == 0
  end

  def self.yum_erase(package)
    # Create an SIM for testing "yum erase-n"
    # https://issues.amazon.com/issues/BEANSTALK-15579
    return system("yum erase -y #{package}")
  end

  # downloads a file from the web to a location on disk
  def self.wget(uri, file_path)
    raise "uri may not be nil" if uri.nil?
    raise "file_path may not be nil"  if file_path.nil?

    open(uri) do |response|
       File.open(file_path,"wb") do |file|
          file.write(response.read)
       end
    end
  end

  def self.download_bundled_app(app_name, directory)
    downloaded_file = File.join('/tmp', app_name)

    unless File.exists?(downloaded_file)
      app_bundle_url = Container.container_config('eb_bundles_url') + app_name
      # TODO: Configure the amazon cacerts, and use Container.wget instead
      run_command("/usr/bin/wget --directory-prefix=/tmp/ #{app_bundle_url} --no-check-certificate", false, false)
    end

    if app_name.end_with?('.tar.gz') || app_name.end_with?('.tgz')
      run_command("/bin/tar -xzf #{downloaded_file} -C #{directory}")
    elsif app_name.end_with?('.zip')
      run_command("/usr/bin/unzip #{downloaded_file} -d #{directory}")
    else
      FileUtils.cp(downloaded_file, directory)
    end

    FileUtils.rm_rf downloaded_file
  end

  def self.install_bundled_python_app(app_archive_name, app_name)
    tmp_dir = '/tmp'
    app_dir = File.join(tmp_dir, app_name)

    download_bundled_app(app_archive_name, tmp_dir)

    Dir.chdir app_dir do
      run_command('python setup.py install')
    end

    FileUtils.rm_rf app_dir
  end

  def self.install_bundled_app(app_archive_name, app_name, app_desired_name)
    install_dir     = File.join('/usr', 'local')
    bin_dir         = File.join('/usr', 'bin')
    app_dir         = File.join(install_dir, app_name)
    app_desired_dir = File.join(install_dir, app_desired_name)
    app_bin_dir     = File.join(app_dir, 'bin')

    download_bundled_app(app_archive_name, install_dir)

    # Create a soft link so that we can reference this app regardless of version
    FileUtils.ln_s(app_dir, app_desired_dir) unless File.exists? app_desired_dir

    binaries = Dir.entries(app_bin_dir) - ['.', '..']
    binaries.each do |binary|
      symlink = File.join(bin_dir, binary)
      FileUtils.ln_s(File.join(app_bin_dir, binary), symlink) unless File.exists? symlink
    end
  end

  def self.configure_logs(name, log_path)
    run_command("/opt/elasticbeanstalk/bin/log-conf -n #{name} -l'#{log_path}'")
  end

  def self.configure_bundle_logs(name, log_path)
    run_command("/opt/elasticbeanstalk/bin/log-conf -n #{name} -l'#{log_path}' -t bundlelogs")
  end
  
  def self.configure_publish_logs(name, log_path)
    run_command("/opt/elasticbeanstalk/bin/log-conf -n #{name} -l'#{log_path}' -t publishlogs")
  end

  # This method marks the instance / AMI as baked
  def self.bake_ami(manifest)
    time = Time.now.strftime("%d/%m/%Y %H:%M")
    run_command("/bin/echo #{time} > #{MANIFEST_PATH}/#{manifest}")
  end

  # This method allows you to post an INFO message to the event stream
  def self.emit_info(message)
    emit_message(message, 'TRACE')
  end

  # This method allows you to post a WARN message to the event stream
  def self.emit_warning(message)
    emit_message(message, 'WARN')
  end

  # This method allows you to post an ERROR message to the event stream
  def self.emit_error(message)
    emit_message(message, 'ERROR')
    abort message
  end

  # This method allows you to run a command and print its output to stdout
  # It returns a Process::Status object
  def self.run_command(command, emit_event=true, echo=true)
    if command.nil?
      raise "Tried to call run_command with a nil command."
    end

    if echo
      puts "Executing: #{command}"
    end

    output, status = Open3.capture2e(command)

    if echo
      puts output
    end

    unless status.success?
      if emit_event == true
        message = "Failed to execute '#{command}'"
        emit_error(message)
      end
    end

    return output, status
  end

  # This method allows you to run a command and print its output to stdout
  # It returns a Process::Status object
  def self.run_command_as(command, user, emit_event=true, echo=true)
    run_command("/sbin/runuser -s '/bin/sh' -l #{user} -c '#{command}'", emit_event, echo)
  end

  def self.healthd_enabled?
    File.directory? '/etc/healthd'
  end

  def self.container_config(property, options={})
    if property.nil?
      raise 'Property name cannot be nil.'
    end

    property_value = config.container_config(property)

    if options[:nil_message]
      raise options[:nil_message] if property_value.nil?
    end

    def self.healthd_unmonitor_application(application_name)
      if healthd_enabled?
        # Emit event to the event stream
        emit_event=false
        # Don't echo command to stdout
        echo=false
        # This is the only way to unmonitor a process at present
        run_command("rm -f /var/elasticbeanstalk/healthd/#{application_name}.pid 1>&2", emit_event, echo)
      end
    end

    if options[:required_file_message]
      raise options[:required_file_message] unless File.exists? property_value
    end

    if options[:require_int_message]
      # Get rid of extraneous spaces
      property_value.strip!
      raise "#{options[:require_int_message]}: #{property_value}" unless property_value.match(/\A\d+\z/)
    end

    property_value
  end

  def self.run_as_user
    container_config('app_user', {:nil_message => "Unable to determine the 'run as' user."})
  end

  def self.container_staging_directory
    container_config('container_staging_dir', {:nil_message => 'Could not determine the location of the container staging directory.'})
  end

  def self.staging_directory
    container_config('app_staging_dir', {:nil_message => 'Could not determine the location of the application staging directory.'})
  end

  def self.application_directory
    container_config('app_deploy_dir', {:nil_message => 'Unable to determine the location of the application directory.'})
  end

  def self.source_bundle
    container_config('source_bundle', {:nil_message => 'Could not determine the location of the source bundle.', :required_file_message => 'Unable to locate the source bundle.'})
  end

  def self.instance_port
    container_config('instance_port', {:nil_message => 'Unable to determine the ELB port.', :require_int_message => "LoadBalancerHTTPPort is not a valid integer"})
  end

  def self.restart_healthd
    run_command('/opt/elasticbeanstalk/bin/healthd-restart')
  end

  def self.is_baked?(manifest)
    if manifest.nil? || manifest.strip.empty?
      raise ArgumentError.new "Invalid manifest name."
    end

    File.exists? File.join(MANIFEST_PATH, manifest)
  end

  def self.environment_variables
    config.environment_variables
  end

  def self.ulimit
    user_limit = MIN_ULIMIT

    if File.exists? FILE_MAX
      file_max = IO.read(FILE_MAX).to_i

      # File format has changed?
      return user_limit if file_max <= 0

      user_limit = file_max / 3

      if user_limit < MIN_ULIMIT
        user_limit = MIN_ULIMIT
      elsif user_limit > MAX_ULIMIT
        user_limit = MAX_ULIMIT
      end
    end

    user_limit
  end

  def self.initctl_start(process)
    run_command("initctl start #{process}")
  end
  
  def self.initctl_stop(process)
    # stop process if it is running
    run_command("if ( initctl status #{process} | grep start ); then initctl stop #{process}; fi")
  end

  private
  def self.with_validation(manifest, packages)
    validate_packages packages
    if is_baked? manifest
      puts "#{manifest.capitalize} dependencies have already been installed."
    else
      yield
    end
  end

  def self.validate_packages(packages)
    if packages.nil? || packages.length < 1
      raise ArgumentError.new "At least one package name must be provided."
    end

    packages.each do |package|
      if package.nil? || package.strip.empty?
        raise ArgumentError.new "Unable to install invalid package '#{package}'."
      end
    end
  end

  def self.emit_message(message, severity)
    unless message.nil? || message.strip.empty? || severity.nil? || severity.strip.empty?
      puts message
      run_command("/usr/bin/eventHelper.py --msg \"#{message}\" --severity #{severity}", false, false)
    end
  end

  # Gets an instance of the container config manager
  # This is required for unit testing, and the container has a different path
  def self.config
    require 'elasticbeanstalk/container-config-manager'
    if File.file? CONTAINER_CONFIGURATION
      ElasticBeanstalk::ContainerConfigManager.new
    else
      ElasticBeanstalk::ContainerConfigManager.new(BAKING_CONFIGURATION)
    end
  end
end
