require 'fileutils'
require 'json'
require 'container'
require 'component'
require 'proxy'
require 'erb'

class NginxComponent < Component
  DEFAULT_CUSTOMER_CONFIG_DIRECTORY = '.ebextensions/nginx'

  NGINX_CONFIG_TEMPLATE    = '/opt/elasticbeanstalk/private/nginx/nginx.conf.erb'
  STATIC_LOCATION_TEMPLATE = '/opt/elasticbeanstalk/private/nginx/static_mapping.conf.erb'
  APP_LOCATION_TEMPLATE    = '/opt/elasticbeanstalk/private/nginx/app_mapping.conf.erb'
  HEALTHD_CONFIG           = '/opt/elasticbeanstalk/private/nginx/healthd.conf'
  HEALTHD_HTTP_CONFIG      = '/opt/elasticbeanstalk/private/nginx/healthd_http.conf'
  HEALTHD_LOG_DIRECTORY    = '/var/log/nginx/healthd/'

  PACKAGE_NAME = 'nginx'

  def self.configure(customer_nginx_config=DEFAULT_CUSTOMER_CONFIG_DIRECTORY)
    super() do
      customer_nginx_config = DEFAULT_CUSTOMER_CONFIG_DIRECTORY if customer_nginx_config.nil?

      # Prepare the staging directory
      FileUtils.rm_rf(nginx_staging_directory)
      FileUtils.mkdir_p(nginx_staging_directory)
      FileUtils.mkdir_p(nginx_eb_conf_staging_directory)

      # We always create the eb locations, which we put in a nonstandard directory
      # /etc/nginx/conf.d/elasticbeanstalk/
      # That way a customer can provide a custom nginx.conf and not have our locations
      # imported unless they explictly do so.
      create_eb_locations

      # Configure nginx.conf
      configure_nginx(customer_nginx_config)

      # Validate the staged config
      Container.run_command("/usr/sbin/nginx -t -c #{eb_nginx_staging_config}")
    end
  end

  def self.start
    super do
      unless File.exists? (File.join(nginx_staging_directory, 'nginx.conf'))
        puts "Stopping Nginx to apply new configuration."
        raise "Unable to locate the Nginx staging config '#{nginx_staging_directory}/nginx.conf'."
      end

      # Stop nginx
      Container.run_command('service nginx stop')

      # Ensure that we remove files that the customer no longer wants
      FileUtils.rm_rf('/etc/nginx/conf.d/')

      # Copy the staging configs to live
      FileUtils.cp_r(File.join(nginx_staging_directory, '.'), '/etc/nginx/')

      # Start nginx
      Container.run_command('service nginx start')
    end
  end

  def self.stop
    super do
      Container.run_command('service nginx stop')
    end
  end

  def self.uninstall_dependencies
    super do
    end
  end

  def self.reinstall_dependencies
    super do
    end
  end

  def self.install_dependencies
    super do
      Container.yum_install_packages(PACKAGE_NAME, 'nginx') unless Container.yum_installed?('nginx')
      # Create a directory for healthd logging
      FileUtils.mkdir_p(HEALTHD_LOG_DIRECTORY)
      Container.run_command("/bin/chown nginx:nginx #{HEALTHD_LOG_DIRECTORY}")
      Container.run_command("/bin/chmod +r #{HEALTHD_LOG_DIRECTORY}")

      # Remove the 'generic' log rotation configuration in favor of the beanstalk managed one
      FileUtils.rm_rf('/etc/logrotate.d/nginx')
    end
  end

  private
  def self.configure_nginx(customer_provided_config)
    Container.configure_logs('nginx', '/var/log/nginx/*')

    customer_nginx_config = "#{Container.staging_directory}/#{customer_provided_config}"

    # If the customer provided their own nginx config then we don't create ours
    if File.exists? "#{customer_nginx_config}/nginx.conf"
      # Let the customer know that we found their config and are using it.
      Container.emit_info("Nginx configuration detected in the '#{customer_provided_config}' directory. AWS Elastic Beanstalk will no longer manage the Nginx configuration for this environment.")
    else
      create_eb_config
    end

    if Container.healthd_enabled?
      FileUtils.cp HEALTHD_CONFIG, nginx_eb_conf_staging_directory
      FileUtils.cp HEALTHD_HTTP_CONFIG, nginx_staging_conf_directory
    end

    if File.directory? customer_nginx_config
      # Copy the customer's config to the staging directory
      FileUtils.cp_r("#{customer_nginx_config}/.", nginx_staging_directory)
    end
  end

  def self.assert_eb_nginx_template_exists
    unless File.exists? NGINX_CONFIG_TEMPLATE
      # Something went wrong, either the customer deleted this directory, or the container is broken
      raise "Unable to locate the Elastic Beanstalk provided Nginx configuration template '#{NGINX_CONFIG_TEMPLATE}'"
    end
  end

  def self.create_eb_config
    assert_eb_nginx_template_exists

    # The load balancer port
    instance_port = Container.instance_port
    user_file_limit = Container.ulimit
    gzip_toggle = ProxyComponent.gzip_toggle

    # Update the server with the ELB port and ulimit
    erb = ERB.new(IO.read(NGINX_CONFIG_TEMPLATE))
    # Save the updated config to staging
    IO.write(eb_nginx_staging_config, erb.result(binding))
  end

  # Accepts a virtual path, and a port to map that path to
  def self.create_port_mapping(virtual_path, application_port)
    erb = ERB.new(IO.read(APP_LOCATION_TEMPLATE))
    erb.result(binding)
  end

  # Accepts a mapping of the form virtual=physical
  def self.create_static_mapping(mapping)
    # Default value for static mapping as the template
    # does not support null values.
    if "" == mapping
      return ""
    end

    if mapping.nil? || !(mapping.include? '=')
      # This is not possible, but adding a sanity check anyway
      # At least if the customer does tinker with things then
      # this message should be more human readable.
      raise "Found invalid static mapping '#{mapping}'"
    end

    mappings = mapping.strip.split('=', 2)
    virtual_path = mappings[0].strip
    physical_path = mappings[1].strip

    if virtual_path.empty? || physical_path.empty?
      raise "Found invalid static mapping '#{mapping}'"
    end

    unless physical_path.start_with?('/')
      physical_path = File.join(Container.application_directory, physical_path)
    end
    
    erb = ERB.new(IO.read(STATIC_LOCATION_TEMPLATE))
    erb.result(binding)
  end

  def self.create_port_mappings
    # Check that the Elastic Beanstalk provided templates are on the disk
    unless File.exists? APP_LOCATION_TEMPLATE
      raise("Unable to locate the Elastic Beanstalk application mappings template '#{APP_LOCATION_TEMPLATE}'.")
    end

    # Ensure that the Procfile component created the port mappings
    unless File.exists? port_mappings_file
      raise "Unable to locate application port mappings file '#{port_mappings_file}'"
    end

    port_mappings = JSON.parse(IO.read port_mappings_file)
    locations     = ""

    # Generate the locations block for apps
    # This will result in a newline at the top of the file, which is ok.
    port_mappings.each_pair do |path, port|
      locations << "#{create_port_mapping(path, port)}\n"
    end

    IO.write eb_nginx_app_locations_file, locations
  end

  def self.create_static_mappings
    # Check that the Elastic Beanstalk provided templates are on the disk
    unless File.exists? STATIC_LOCATION_TEMPLATE
      raise("Unable to locate the Elastic Beanstalk static mappings template '#{STATIC_LOCATION_TEMPLATE}'.")
    end

    locations = ""
    # Customer provided static mappings
    static_files = ProxyComponent.static_files

    unless static_files.nil?
      static_files.each do |mapping|
        locations << "#{create_static_mapping(mapping)}\n"
      end
      IO.write eb_nginx_static_locations_file, locations
    end
  end

  # Configures the elastic beanstalk provided nginx config
  def self.create_eb_locations
    create_port_mappings
    create_static_mappings
  end

  def self.package_name
    return PACKAGE_NAME
  end

  def self.container_staging_directory
    Container.container_staging_directory
  end

  def self.proxy_staging_directory
    "#{container_staging_directory}/proxy"
  end

  def self.port_mappings_file
    "#{proxy_staging_directory}/mappings"
  end

  def self.nginx_staging_directory
    "#{container_staging_directory}/nginx"
  end

  def self.nginx_staging_conf_directory
    "#{nginx_staging_directory}/conf.d"
  end

  def self.eb_nginx_staging_config
    "#{nginx_staging_directory}/nginx.conf"
  end

  def self.nginx_eb_conf_staging_directory
    "#{nginx_staging_directory}/conf.d/elasticbeanstalk"
  end

  def self.eb_nginx_app_locations_file
    "#{nginx_eb_conf_staging_directory}/00_application.conf"
  end

  def self.eb_nginx_static_locations_file
    "#{nginx_eb_conf_staging_directory}/01_static.conf"
  end
end

module ProxyServer
  module Nginx
    NAME_MAPPING = {'nginx' => ::NginxComponent}
  end
end
