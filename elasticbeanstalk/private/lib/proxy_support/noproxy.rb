require 'fileutils'
require 'json'
require 'container'

class NoProxyComponent < Component

  PACKAGE_NAME = 'NoProxy'

  def self.configure(customer_config=nil)
    super do
    end
  end

  # Can set ip table rules here
  def self.start
    super do
      puts "Not starting the proxy as it has been disabled"

      # Configure health
      if Container.healthd_enabled?
        Container.run_command('/bin/rm -rf /var/elasticbeanstalk/healthd/proxy.pid')
        Container.run_command("/opt/elasticbeanstalk/bin/healthd-configure --appstat-log-path '' --appstat-unit '' --appstat-timestamp-on ''")
      end
    end
  end

  # Can remove ip table rules here
  def self.stop
    super do
    end
  end

  def self.uninstall_dependencies
    super do
    end
  end

  def self.install_dependencies
    super do
    end
  end

  def self.reinstall_dependencies
    super do
    end
  end

  def self.package_name
    PACKAGE_NAME
  end
end

module ProxyServer
  module NoProxy
    NAME_MAPPING = {'noproxy' => ::NoProxyComponent}
  end
end
