require 'fileutils'
require 'json'
require 'container'
require 'component'
require 'proxy_constants/httpd_constants'

class HttpdComponent < Component
  DEFAULT_CUSTOMER_CONFIG_DIRECTORY = '.ebextensions/httpd'

  HTTPD_SYSCONFIG          = '/opt/elasticbeanstalk/private/httpd22/httpd'
  HTTPD_CONFIG             = '/opt/elasticbeanstalk/private/httpd22/httpd.conf'
  STATIC_LOCATION_TEMPLATE = '/opt/elasticbeanstalk/private/httpd22/static_mapping.conf.erb'
  APP_LOCATION_TEMPLATE    = '/opt/elasticbeanstalk/private/httpd22/app_mapping.conf.erb'
  GZIP_CONFIG              = '/opt/elasticbeanstalk/private/httpd22/gzip.conf'
  HEALTHD_CONFIG           = '/opt/elasticbeanstalk/private/httpd22/healthd.conf'
  BEANSTALK_LOG_CONFIG     = '/opt/elasticbeanstalk/private/httpd22/elasticbeanstalk_log.conf'
  HEALTHD_LOG_DIRECTORY    = '/var/log/httpd/healthd/'
  CONTAINER_CONFIG_DIRECTORY = '/opt/elasticbeanstalk/private/httpd22/container_specific_conf/'

  PACKAGE_NAME = 'httpd'
  HTTPD_CONFIG_DIRECTORY = '/etc/httpd/'
  HTTPD_TOOLS_PACKAGE_NAME = 'httpd-tools'
  HTTPD_DEPENDENCY_NAME = 'apr-util-ldap'
  MOD_SSL_NAME = 'mod_ssl'

  def self.configure(customer_httpd_config = DEFAULT_CUSTOMER_CONFIG_DIRECTORY)
    super() do
      customer_httpd_config ||= DEFAULT_CUSTOMER_CONFIG_DIRECTORY

      # Prepare the staging directory
      FileUtils.rm_rf(HttpdConstants.httpd_staging_directory)
      FileUtils.mkdir_p(HttpdConstants.httpd_staging_directory)
      FileUtils.mkdir_p(HttpdConstants.httpd_staging_conf_directory)
      FileUtils.mkdir_p(HttpdConstants.httpd_eb_conf_staging_directory)

      # We always create the eb locations, which we put in a nonstandard directory
      # /etc/httpd/conf.d/elasticbeanstalk/
      # That way a customer can provide a custom httpd.conf and not have our locations
      # imported unless they explictly do so.
      create_eb_locations

      # Configure /etc/httpd/conf.d/*.conf
      configure_httpd customer_httpd_config

      # Validate the staged config
      FileUtils.mkdir_p('/etc/httpd/conf.d/elasticbeanstalk')
      Container.run_command("/usr/sbin/apachectl -t -f #{HttpdConstants.eb_httpd_staging_config}")

      unless File.exists? (File.join(HttpdConstants.httpd_staging_conf_directory, 'httpd.conf'))
        raise "Unable to locate the Httpd staging config '#{HttpdConstants.httpd_staging_conf_directory}/httpd.conf'."
      end

      # Ensure that we remove files that the customer no longer wants
      FileUtils.rm_rf('/etc/httpd/conf.d/')

      # Copy the staging configs to live
      FileUtils.cp_r(File.join(HttpdConstants.httpd_staging_directory, '.'), '/etc/httpd/')
    end
  end

  def self.start
    super do
      # Stop httpd
      Container.run_command('service httpd stop')
      # Start httpd
      Container.run_command('service httpd start')
    end
  end

  def self.stop
    super do
      Container.run_command('service httpd stop') if Container.yum_installed?(PACKAGE_NAME)
    end
  end

  def self.uninstall_dependencies
    super do
      unless HttpdConstants.proxy_type.eql? HttpdConstants.proxy_server_apache_22
        if Container.yum_installed?(PACKAGE_NAME)
          Container.yum_erase(PACKAGE_NAME)
          FileUtils.rm_rf(HTTPD_CONFIG_DIRECTORY)
          Container.yum_erase(HTTPD_TOOLS_PACKAGE_NAME)
          Container.yum_erase(HTTPD_DEPENDENCY_NAME)
          Container.yum_erase(MOD_SSL_NAME)
        end
      end
    end
  end

  def self.install_dependencies

      reinstall_dependencies
  end

  def self.reinstall_dependencies
    super do
      if HttpdConstants.proxy_type.eql? HttpdConstants.proxy_server_apache_22
        Container.reinstall_packages(PACKAGE_NAME) unless Container.yum_installed?(PACKAGE_NAME)
        Container.reinstall_packages(MOD_SSL_NAME) unless Container.yum_installed?(MOD_SSL_NAME)

        install_dependencies_helper
      end
    end
  end

  def self.install_dependencies_helper
    Container.run_command("/bin/chown root:apache /var/run/httpd")
    Container.run_command("/bin/chmod 755 /var/run/httpd")
    Container.run_command("/bin/chown apache:apache /var/log/httpd")
    Container.run_command("/bin/chmod 755 /var/log/httpd")

    # Create a directory for healthd logging
    unless Dir.exists? HEALTHD_LOG_DIRECTORY
      FileUtils.mkdir_p(HEALTHD_LOG_DIRECTORY)
      Container.run_command("/bin/chown apache:apache #{HEALTHD_LOG_DIRECTORY}")
      Container.run_command("/bin/chmod +r #{HEALTHD_LOG_DIRECTORY}")
    end

    # Remove the 'generic' log rotation configuration in favor of the beanstalk managed one
    FileUtils.rm_rf('/etc/logrotate.d/httpd')
  end

  private
  def self.configure_httpd(customer_provided_config)
    Container.configure_logs(PACKAGE_NAME, '/var/log/httpd/*')

    customer_httpd_config = "#{Container.staging_directory}/#{customer_provided_config}"

    # If the customer provided their own httpd config then we don't create ours
    if File.exists? "#{customer_httpd_config}/conf/httpd.conf"
      # Let the customer know that we found their config and are using it.
      Container.emit_info("Httpd configuration detected in the '#{customer_provided_config}' directory. AWS Elastic Beanstalk will no longer manage the httpd configuration for this environment.")
    else
      create_eb_httpd_config
    end

    if File.exists? '/usr/sbin/httpd.worker'
      FileUtils.cp(HTTPD_SYSCONFIG, '/etc/sysconfig/')
    end

    generate_gzip

    generate_static_mappings

    if Container.healthd_enabled?
      FileUtils.cp(HEALTHD_CONFIG, HttpdConstants.httpd_staging_confd_directory)
    end

    if File.directory? CONTAINER_CONFIG_DIRECTORY
      FileUtils.cp_r(File.join(CONTAINER_CONFIG_DIRECTORY, '.'), HttpdConstants.httpd_eb_conf_staging_directory)
    end

    if File.directory? customer_httpd_config
      # Copy the customer's config to the staging directory
      FileUtils.cp_r(File.join(customer_httpd_config, '.'), HttpdConstants.httpd_staging_directory)
    end
  end

  def self.create_eb_httpd_config
    unless File.exists? HTTPD_CONFIG
      # Something went wrong, either the customer deleted this directory, or the container is broken
      raise "Unable to locate the Elastic Beanstalk provided Httpd configuration template '#{HTTPD_CONFIG}'"
    end
    FileUtils.cp(HTTPD_CONFIG, HttpdConstants.eb_httpd_staging_config)
    FileUtils.cp(BEANSTALK_LOG_CONFIG, HttpdConstants.httpd_staging_confd_directory)
  end

  # Accepts a virtual path, and a port to map that path to
  def self.generate_eb_application_config(path, port)
    virtual_path = path.to_s
    application_port = port.to_s
    # The load balancer port
    instance_port = Container.instance_port
    erb = ERB.new(IO.read(APP_LOCATION_TEMPLATE))

    erb.result binding
  end

  # Accepts a mapping of the form virtual=physical
  def self.generate_static_mapping(mapping)
    # [""] is the default value for static mapping as the template
    # does not support null values.
    # nil is evaluated as false in ruby
    return "" if mapping && mapping.empty?

    if mapping.nil? || !(mapping.include? '=')
      # This is not possible, but adding a sanity check anyway
      # At least if the customer does tinker with things then
      # this message should be more human readable.
      raise "Found invalid static mapping '#{mapping}'"
    end

    virtual_path, _, physical_path = mapping.strip.partition('=').collect{|v| v.strip || v}

    if virtual_path.empty? || physical_path.empty?
      raise "Found invalid static mapping '#{mapping}'"
    end

    unless physical_path.start_with?('/')
      physical_path = File.join(Container.application_directory, physical_path)
    end

    template = IO.read STATIC_LOCATION_TEMPLATE
    erb = ERB.new(IO.read(STATIC_LOCATION_TEMPLATE))
    erb.result binding
  end

  def self.generate_static_mappings
    # Check that the Elastic Beanstalk provided templates are on the disk
    unless File.exists? STATIC_LOCATION_TEMPLATE
      raise("Unable to locate the Elastic Beanstalk static mappings template '#{STATIC_LOCATION_TEMPLATE}'.")
    end

    # Customer provided static mappings
    static_files = ProxyComponent.static_files

    unless static_files.nil?
      locations = static_files.map { |mapping| generate_static_mapping mapping }
      # Write the static file mappings customer provided to eb static location file:
      # /var/elasticbeanstalk/staging/httpd/conf.d/elasticbeanstalk/02_static.conf
      # The static file configures proxy server to serve static files.
      IO.write(HttpdConstants.eb_httpd_static_locations_file, locations.insert(0, '#Elastic Beanstalk Managed').join("\n"))
    end

  end

  def self.generate_gzip
    unless File.exists? GZIP_CONFIG
      raise("Unable to locate the Elastic Beanstalk gzip compression template '#{GZIP_CONFIG}'.")
    else
      if ProxyComponent.gzip_toggle == 'on'
        FileUtils.cp GZIP_CONFIG, HttpdConstants.eb_httpd_gzip_file
      end
    end
  end

  # Configures the elastic beanstalk provided httpd config
  def self.create_eb_locations
    # Check that the Elastic Beanstalk provided templates are on the disk
    unless File.exists? APP_LOCATION_TEMPLATE
      raise("Unable to locate the Elastic Beanstalk application mappings template '#{APP_LOCATION_TEMPLATE}'.")
    end

    # Ensure that the Procfile component created the port mappings
    unless File.exists? HttpdConstants.port_mappings_file
      raise "Unable to locate application port mappings file '#{HttpdConstants.port_mappings_file}'"
    end

    locations     = "# Elastic Beanstalk Managed\n"
    port_mappings = JSON.parse(IO.read HttpdConstants.port_mappings_file)

    # Generate the locations block for apps
    # This will result in a newline at the top of the file, which is ok.
    port_mappings.each_pair do |path, port|
      locations << "#{generate_eb_application_config(path, port)}\n"
    end

    IO.write HttpdConstants.eb_httpd_app_locations_file, locations
  end

  def self.package_name
    return PACKAGE_NAME
  end
end

module ProxyServer
  module Httpd
    NAME_MAPPING = {HttpdConstants.proxy_server_apache_22 => ::HttpdComponent}
  end
end
