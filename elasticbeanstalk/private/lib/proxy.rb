require 'fileutils'
require 'json'
require 'component'
require 'container'
Dir["#{File.dirname(File.realpath(__FILE__))}/proxy_support/*.rb"].each { |file| require file }

class ProxyComponent < Component
  HEALTHD_STATE_DIRECTORY = '/var/elasticbeanstalk/healthd/'
  HEALTHD_CURRENT_PROXY   = "#{HEALTHD_STATE_DIRECTORY}/current_proxy_server"
  PROXY_NAMESPACE = "aws:elasticbeanstalk:environment:proxy"
  PROXY_SERVER = "ProxyServer"
  PROXY_STATIC_FILES = "StaticFiles"
  PROXY_GZIP_TOGGLE = "GzipCompression"
  PACKAGE_NAME = 'Proxy'
  PROXY_SERVER_NGINX = 'nginx'
  PROXY_SERVER_APACHE = 'apache'
  PROXY_SERVER_APACHE_22 ='apache/2.2'
  PROXY_SERVER_HTTPD = 'httpd'
  PROXY_VALUE_HTTPD_24 = 'httpd2.4'
  PROXY_VALUE_HTTPD_22 = 'httpd2.2'

  def self.configure(customer_config=nil)
    super() do
      create_application_mappings
      proxy_map.values.each { |pxy| pxy.uninstall_dependencies }
      proxy_map.values.each { |pxy| pxy.reinstall_dependencies }
      proxy.configure(customer_config)
    end
  end

  def self.start
    super do
      proxy.start
      restart_healthd
    end
  end

  # Note stop should always stop all known proxies. If a
  # customer changes proxies, then you may end up leaving
  # the old proxy up and running.
  def self.stop
    super do
      proxy_map.values.each { |pxy| pxy.stop }
    end
  end

  # Include all known proxy dependencies
  def self.install_dependencies
    # Add this uninstall and reinstall logic here to deal if customer
    # has ebextensions that will have conflicts with default proxy server version installed.
    # https://issues.amazon.com/issues/BEANSTALK-15881
    # Move uninstall_dependencies in ProxyComponent instead of HttpdComponent and Httpd24Component.
    # https://issues.amazon.com/issues/BEANSTALK-16406
    proxy_map.values.each { |pxy| pxy.uninstall_dependencies }
    proxy_map.values.each { |pxy| pxy.install_dependencies }
  end

  private
  def self.create_application_mappings
    application_mappings = get_application_mappings(get_base_port)

    # We create the directory in case it does not exist
    FileUtils.mkdir_p proxy_staging_directory
    IO.write port_mappings_file, application_mappings.to_json
  end

  def self.get_application_mappings(base_port)
    base_app = '/web'
    application_mappings = {}
    application_mappings[base_app] = base_port

    # Get the application port mappings
    procfile = "#{Container.staging_directory}/Procfile"

    # Find all web applications
    if File.exists? procfile
      File.open(procfile) do |file|
        file.each_line do |line|
          web_app = get_web_app(line)
          # Add a port mapping if this is a web app
          unless web_app.nil?
            application_mappings["/#{web_app}"] = base_port
          end
          # Every app in order bumps up the port by 100, even non web app ones
          if line.match(/^([A-Za-z0-9_]+):\s*(.+)$/)
            base_port += 100
          end
        end
      end
    end

    # web is root
    # other apps e.g. (web_appname) are accessible under /appname
    application_mappings['/'] = application_mappings[base_app]
    application_mappings.delete(base_app)
    return application_mappings
  end

  # Get this from foreman itself
  def self.get_base_port
    base_port = Container.container_config('application_port').to_i
    base_port = base_port > 0 ? base_port : 5000
    foreman_file = "#{Container.staging_directory}/.foreman"

    # foreman allows duplicates, and only uses the last occurance
    if File.exists? foreman_file
      port = IO.readlines(foreman_file).map {|line| get_port(line)}.compact.last
      base_port = port unless port.nil?
    end

    base_port
  end

  def self.get_web_app(app)
    app.downcase.match(/^(web):.+$/)[1] rescue nil
  end

  def self.get_port(port)
    port.downcase.match(/^\s*port\s*:\s*([0-9]+)\s*$/)[1].to_i rescue nil
  end

  # Alway restart healthd service, as the setting of healthd configuration is not correct when the instance reboot.
  # TODO: After we use new logic in bootstrap script to set healthd configuration, change this logic back to restart
  # healthd when there has proxy change.
  def self.restart_healthd
    new_proxy = proxy_type
    new_proxy_value = proxy_type

    if new_proxy == PROXY_SERVER_APACHE
      new_proxy = PROXY_SERVER_HTTPD
      new_proxy_value = PROXY_VALUE_HTTPD_24
    end

    if new_proxy == PROXY_SERVER_APACHE_22
      new_proxy = PROXY_SERVER_HTTPD
      new_proxy_value = PROXY_VALUE_HTTPD_22
    end

    set_current_proxy_type new_proxy_value
    healthd_monitor_proxy new_proxy
    Container.restart_healthd
  end

  def self.healthd_monitor_proxy(proxy_name)
    if Container.healthd_enabled?
      if proxy_name.eql? 'none'
        healthd_unmonitor_proxy
      else
        if proxy != nil
          Container.run_command("/bin/chmod 755 /var/run/#{proxy_name}") if File.directory? ("/var/run/#{proxy_name}")
          Container.run_command("/opt/elasticbeanstalk/bin/healthd-track-pidfile --proxy #{proxy_name}")
          case proxy_name
          when PROXY_SERVER_HTTPD
            appstat_unit = 'usec'
            appstat_timestamp_on = 'arrival'
          when PROXY_SERVER_NGINX
            appstat_unit = 'sec'
            appstat_timestamp_on = 'completion'
          end
          Container.run_command("/opt/elasticbeanstalk/bin/healthd-configure --appstat-log-path /var/log/#{proxy_name}/healthd/application.log --appstat-unit #{appstat_unit} --appstat-timestamp-on '#{appstat_timestamp_on}'")
        else
          raise "Unsupported proxy '#{proxy_name}'."
        end
      end
    end
  end

  def self.healthd_unmonitor_proxy
    if Container.healthd_enabled?
      Container.run_command('rm -f /var/elasticbeanstalk/healthd/proxy.pid')
    end
  end

  def self.current_proxy_type
    return nil unless File.exists? HEALTHD_CURRENT_PROXY

    IO.read(HEALTHD_CURRENT_PROXY).strip
  end

  def self.set_current_proxy_type(new_proxy)
    FileUtils.mkdir_p('/var/elasticbeanstalk/healthd/')

    IO.write HEALTHD_CURRENT_PROXY, new_proxy
  end

  def self.gzip_toggle
    begin
      gzip_toggle = Container.config.optionsetting(PROXY_NAMESPACE, PROXY_GZIP_TOGGLE)
    rescue RuntimeError
      return "off"
    end
    return "on" if gzip_toggle == "true"
    return "off" if gzip_toggle == "false"
    raise "invalid Gzip compression setting"
  end

  def self.static_files
    begin
      Container.config.optionsetting(PROXY_NAMESPACE, PROXY_STATIC_FILES)
    rescue RuntimeError
      Container.container_config('static_files')
    end
  end

  def self.proxy(current_type = proxy_type)
    raise "Unsupported proxy type: #{current_type}" if proxy_map[current_type].nil?

    proxy_map[current_type]
  end

  def self.proxy_map
    mapping_list = ProxyServer.constants.map do |con|
      ProxyServer.const_get(con).const_get(:NAME_MAPPING)
    end
    mapping_list.compact.reduce(Hash.new, :merge)
  end

  def self.proxy_type
    begin
      Container.config.optionsetting(PROXY_NAMESPACE, PROXY_SERVER).strip
    rescue ElasticBeanstalk::BeanstalkRuntimeError
      Container.container_config('proxy_server').strip
    end
  end

  def self.package_name
    PACKAGE_NAME
  end

  def self.container_staging_directory
    Container.container_staging_directory
  end

  def self.proxy_staging_directory
    "#{container_staging_directory}/proxy"
  end

  def self.port_mappings_file
    "#{proxy_staging_directory}/mappings"
  end

end
