#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

EB_APP_DEPLOY_DIR=$(/opt/elasticbeanstalk/bin/get-config  container -k app_deploy_dir)
TOMCAT_VERSION=$(/opt/elasticbeanstalk/bin/get-config  container -k tomcat_version)
if [ $TOMCAT_VERSION == "8.5" ];
then
    TOMCAT_VERSION="8"
fi
TOMCAT_NAME=tomcat$TOMCAT_VERSION

# Create the ROOT directory in case this was removed by a deployment or the customer
# This is required so that readlink works
mkdir -p $EB_APP_DEPLOY_DIR
EB_APP_DEPLOY_BASE_DIR=$(readlink -f $EB_APP_DEPLOY_DIR/..)
# Remove all files and directories on the same level as $EB_APP_DEPLOY_DIR
rm -rf $EB_APP_DEPLOY_BASE_DIR/*
rm -rf /usr/share/$TOMCAT_NAME/conf/Catalina/localhost/*
rm -rf /usr/share/$TOMCAT_NAME/work/Catalina/*

mkdir -p $EB_APP_DEPLOY_DIR
