#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

TOMCAT_VERSION=$(/opt/elasticbeanstalk/bin/get-config container -k tomcat_version)
if [ $TOMCAT_VERSION == "8.5" ];
then
    TOMCAT_VERSION="8"
fi
TOMCAT_NAME=tomcat$TOMCAT_VERSION

if /etc/init.d/$TOMCAT_NAME status
then
  /usr/bin/monit unmonitor tomcat
  /etc/init.d/$TOMCAT_NAME stop
fi
