#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

EB_APP_STAGING_DIR=$(/opt/elasticbeanstalk/bin/get-config  container -k app_staging_dir)
EB_APP_DEPLOY_DIR=$(/opt/elasticbeanstalk/bin/get-config  container -k app_deploy_dir)

# Count files ignoring hidden files
FILE_COUNT=$(find $EB_APP_STAGING_DIR -maxdepth 1 -type f -not -path '*/\.*' | wc -l)
# Get the list of war files
WAR_FILES=$(find $EB_APP_STAGING_DIR -maxdepth 1 -type f | grep -Pi "\.war$" || echo "")
# Count the number of war files
WAR_FILE_COUNT=0
# Count the number of war files if WAR_FILES is not empty
if [[ ${#WAR_FILES} > 0 ]]; then
  WAR_FILE_COUNT=$(echo "$WAR_FILES" | wc -l)
fi
# Find the directory containing the ROOT directory
EB_APP_DEPLOY_BASE=$(readlink -f $EB_APP_DEPLOY_DIR/../)

# This isn't require for the war files
# Also, if this directory exists then it changes the behavior of:
# - 'cp -R $EB_APP_STAGING_DIR $EB_APP_DEPLOY_DIR'
rm -rf $EB_APP_DEPLOY_DIR

# If the staging directory ONLY contains war files then we move it to the tomcat base directory
if [[ $FILE_COUNT == $WAR_FILE_COUNT && $WAR_FILE_COUNT > 1 ]]; then
    for WAR_FILENAME in $WAR_FILES
    do
        # Move all the war files to the base directory
        mv $WAR_FILENAME $EB_APP_DEPLOY_BASE
    done
else
  cp -R $EB_APP_STAGING_DIR $EB_APP_DEPLOY_DIR
fi

chown -R tomcat:tomcat $EB_APP_DEPLOY_BASE
