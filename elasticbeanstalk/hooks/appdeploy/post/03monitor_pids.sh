#!/bin/bash

TOMCAT_VERSION=$(/opt/elasticbeanstalk/bin/get-config container -k tomcat_version)
if [ $TOMCAT_VERSION == "8.5" ];
then
  TOMCAT_VERSION="8"
fi
/opt/elasticbeanstalk/bin/healthd-track-pidfile --name application --location "/var/run/tomcat$TOMCAT_VERSION.pid"

