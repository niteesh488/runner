#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

function is_baked
{
  if [[ -f /etc/elasticbeanstalk/baking_manifest/$1 ]]; then
    true
  else
    false
  fi
}

JAVA_VERSION=$(/opt/elasticbeanstalk/bin/get-config container -k java_version)
TOMCAT_VERSION=$(/opt/elasticbeanstalk/bin/get-config container -k tomcat_version)

if is_baked tomcat_packages; then
	echo tomcat and related packages have already been installed. Skipping installation.
else
 	yum install -y \
	    log4j \
            monit

	case $TOMCAT_VERSION in
      7)
        yum install -y tomcat7
      ;;
      8)
        yum install -y tomcat80
      ;;
      8.5)
        yum install -y tomcat8
      ;;
      *)
	    echo ERROR: Tomcat version $TOMCAT_VERSION is not supported.
	    exit 1
    esac
fi

# Configure the tomcat user's shell
chsh -s /sbin/nologin tomcat

case $JAVA_VERSION in
  6)
	if is_baked java6_jre; then
    	echo Java6 JRE has already been installed. Skipping installation.
	else
		echo "Configuring Java 6"
		yum install -y \
	      java-1.6.0-openjdk
	
		/usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.6.0-openjdk.x86_64/bin/java
	fi
  ;;
  7)
	if is_baked java7_jre; then
		echo Java7 JRE has already been installed. Skipping installation.
	else
		echo "Configuring Java 7"
		yum update -y \
	      java-1.7.0
	fi
  ;;
  8)
	if is_baked java8_jre; then
    	echo Java8 JRE has already been installed. Skipping installation.
	else
		echo "Configuring Java 8"
		yum install -y \
	      java-1.8.0-openjdk
	
		/usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java
	fi
  ;;
  *)
	echo ERROR: Java version $JAVA_VERSION is not supported.
	exit 1
esac
