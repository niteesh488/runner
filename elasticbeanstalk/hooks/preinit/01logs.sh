#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

TOMCAT_VERSION=$(/opt/elasticbeanstalk/bin/get-config container -k tomcat_version)
if [ $TOMCAT_VERSION == "8.5" ];
then
    TOMCAT_VERSION="8"
fi
TOMCAT_NAME=tomcat$TOMCAT_VERSION

if [[ -n "$EB_FIRST_RUN" ]];
then
  #Recreate tomcat log folder
	rm -rf /var/log/$TOMCAT_NAME
	mkdir -p /var/log/$TOMCAT_NAME
	chown -R tomcat:tomcat /var/log/$TOMCAT_NAME
fi

# Warning: any pattern containing "*" must immediately follow leading switch, to prevent Shell expanding it

/opt/elasticbeanstalk/bin/log-conf -n"$TOMCAT_NAME" -l"/var/log/$TOMCAT_NAME/*"
/opt/elasticbeanstalk/bin/log-conf -n'httpd' -l'/var/log/httpd/*' -f /opt/elasticbeanstalk/containerfiles/httpd.logrotate.conf
/opt/elasticbeanstalk/bin/log-conf -t bundlelogs -n 'monit' -l"/var/log/monit*"


echo "Removing tomcat internal log rotation."
/bin/sed -i -e 's|prefix="localhost_access_log.*$|prefix="localhost_access_log" suffix=".txt" rotatable="false"|' /etc/$TOMCAT_NAME/server.xml

echo "Setting logArgs, logProps and logEnv for VersionLoggerListener."
/bin/sed -i -e 's|className="org.apache.catalina.startup.VersionLoggerListener"\s*/>$|className="org.apache.catalina.startup.VersionLoggerListener" \
logArgs="false" logEnv="false" logProps="false" />|' /etc/$TOMCAT_NAME/server.xml

echo "Removing default tomcat logrotate."
/bin/rm -f /etc/logrotate.d/$TOMCAT_NAME

echo "Removing default httpd logrotate."
/bin/rm -f /etc/logrotate.d/httpd

echo "Removing extra log file."
sed -i '/.handlers = 1catalina.org.apache.juli.FileHandler, java.util.logging.ConsoleHandle/c \
.handlers = java.util.logging.ConsoleHandler' /etc/$TOMCAT_NAME/logging.properties
sed -i '/org.apache.catalina.core.ContainerBase.\[Catalina\].\[localhost\].handlers = 2localhost.org.apache.juli.FileHandler/c \
org.apache.catalina.core.ContainerBase.[Catalina].[localhost].handlers = java.util.logging.ConsoleHandler' /etc/$TOMCAT_NAME/logging.properties
