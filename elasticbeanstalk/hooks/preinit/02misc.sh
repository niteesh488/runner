#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

function preinit() {

TOMCAT_VERSION=$(/opt/elasticbeanstalk/bin/get-config container -k tomcat_version)
if [ $TOMCAT_VERSION == "8.5" ];
then
    TOMCAT_VERSION="8"
fi
TOMCAT_NAME=tomcat$TOMCAT_VERSION
  
TOMCAT_HOME=/usr/share/$TOMCAT_NAME
TOMCAT_CONF_HOME=/etc/$TOMCAT_NAME

SET_LIMIT_SH='/etc/elasticbeanstalk/set-ulimit.sh'

echo "Patching Tomcat $TOMCAT_VERSION startup scripts"
if [ -f /opt/elasticbeanstalk/containerfiles/tomcat-elasticbeanstalk ]; then
    echo "Installing tomcat-elasticbeanstalk script"
    /bin/mv /opt/elasticbeanstalk/containerfiles/tomcat-elasticbeanstalk /usr/sbin
    /bin/chown root:root /usr/sbin/tomcat-elasticbeanstalk
    /bin/chmod 755 /usr/sbin/tomcat-elasticbeanstalk
    echo "Fixing Tomcat $TOMCAT_VERSION init.d script"
    /bin/sed -i -e "s/\/usr\/sbin\/$TOMCAT_NAME/\/usr\/sbin\/tomcat-elasticbeanstalk/g" /etc/init.d/$TOMCAT_NAME
    if ! cat /etc/init.d/$TOMCAT_NAME | grep -q "$SET_LIMIT_SH"; then
      sed -i "/function start() {/a \ \ \ if [ -f $SET_LIMIT_SH ]; then . $SET_LIMIT_SH; fi" /etc/init.d/$TOMCAT_NAME
    fi
fi

# Tomcat misc config

## X-Forwarded-For support

echo "Adding X-Forwarded-Proto valve"
/bin/sed -i -e '/<\/Host>/ i\
    <Valve className="org.apache.catalina.valves.RemoteIpValve" protocolHeader="X-Forwarded-Proto" internalProxies="10\\.\\d+\\.\\d+\\.\\d+|192\\.168\\.\\d+\\.\\d+|169\\.254\\.\\d+\\.\\d+|127\\.\\d+\\.\\d+\\.\\d+|172\\.(1[6-9]|2[0-9]|3[0-1])\\.\\d+\\.\\d+" \/>
' $TOMCAT_CONF_HOME/server.xml

echo "Setting UTF-8 support"
if ! grep -q "Elastic Beanstalk" $TOMCAT_CONF_HOME/server.xml ; then
    sed -i '1 a\<!-- Elastic Beanstalk Maintained -->' $TOMCAT_CONF_HOME/server.xml
    sed -i 's/<Connector port=\"8080\" protocol=\"HTTP\/1.1\"/&\n               URIEncoding="UTF-8"/g' $TOMCAT_CONF_HOME/server.xml
fi

# Make sure commons pool is available to tomcat.
ln -sf /usr/share/java/apache-commons-pool.jar /usr/share/$TOMCAT_NAME/lib/
}

if [[ -n "$EB_FIRST_RUN" ]];
then
  echo "Running preinit"
  preinit
else
  echo "Running preinit-reboot"
fi
