#!/bin/bash
#==============================================================================
# Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Amazon Software License (the "License"). You may not use
# this file except in compliance with the License. A copy of the License is
# located at
#
#       https://aws.amazon.com/asl/
#
# or in the "license" file accompanying this file. This file is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
# implied. See the License for the specific language governing permissions
# and limitations under the License.
#==============================================================================

set -xe

EB_APP_STAGING_DIR=$(/opt/elasticbeanstalk/bin/get-config  container -k app_staging_dir)
EB_SOURCE_BUNDLE=$(/opt/elasticbeanstalk/bin/get-config  container -k source_bundle)

/usr/bin/unzip -o -d $EB_APP_STAGING_DIR $EB_SOURCE_BUNDLE

FILE_COUNT=`find $EB_APP_STAGING_DIR -maxdepth 1 -type f | wc -l`
ITEM_COUNT=`ls $EB_APP_STAGING_DIR --almost-all | wc -l`

if [[ $FILE_COUNT == 0  &&  $ITEM_COUNT == 1 ]]; then
    # Found one and only one directory without any other regular file
    DIR_NAME=`ls $EB_APP_STAGING_DIR --almost-all`

    if [[ ! $DIR_NAME =~ \..* ]] && [ "$DIR_NAME" != "WEB-INF" ]; then    # Test if directory is a regular directory without leading .
    # Rename parent directory to avoid name collision
    NEW_PARENT_PATH=$EB_APP_STAGING_DIR/$DIR_NAME$RANDOM$RANDOM
    mv $EB_APP_STAGING_DIR/$DIR_NAME $NEW_PARENT_PATH

        # Move all files and directories one level up
    for NAME in `ls $NEW_PARENT_PATH --almost-all`
        do
            mv $NEW_PARENT_PATH/$NAME $EB_APP_STAGING_DIR/
        done

    # Clean up
        rmdir $NEW_PARENT_PATH
    fi
fi
